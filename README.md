# RF Filter Switch

Early attempt of an usb controlled double rf switch allowing to switch in different filter/lna into an rf path. First poc with 2 path

Idea
- USB powered and controlled using vusb
  - ATTiny85 for dev, 45 or even 25 would be nice to be able to run on
  - Possibly micronucleus bootloader during dev for easy loading of fw over usb. Will probably only work on 45 & 85 unless the vusb code could be reused between bootloader and firmware
- 2 RF switch allowing for 2 or more path between a common input and output
- simple cli tool to control it
  - receiving a single frequency as param
  - using a simple config file to decide which path to take according to frequency
  - optionally specify an alternate config file
  - optionally specify the device through serial numberr
